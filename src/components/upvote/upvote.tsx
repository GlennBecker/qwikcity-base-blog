import { component$, useClientEffect$, useWatch$, useStore } from "@builder.io/qwik";
import { useLocation } from "@builder.io/qwik-city";

export const LOCALSTORAGEKEY = 'qwikcity-post-votes';

export interface UpvoteState {
  vote: boolean | undefined
  allVotes: Record<string, boolean>
}

export const Upvote = component$(() => {
  const pathname = useLocation().pathname;
  const state: UpvoteState = useStore({ vote: undefined, allVotes: {} })
  useClientEffect$(() => {
    state.allVotes = JSON.parse(window.localStorage.getItem(LOCALSTORAGEKEY) || '{}')
    state.vote = state.allVotes[pathname];
  });
  useWatch$(track => {
    const vote = track(state, 'vote');
    if (vote !== undefined) {
      console.log(vote);
      window.localStorage.setItem(LOCALSTORAGEKEY, JSON.stringify({ ...state.allVotes, [pathname]: state.vote }))
    }
  })

  return <div>
    <label><input type="radio" name="vote" onChange$={() => state.vote = true} checked={state.vote === true} aria-label="upvote post" />👍</label>
    <label><input type="radio" name="vote" onChange$={() => state.vote = false} checked={state.vote === false} aria-label="downvote post" />👎</label>
  </div>
})