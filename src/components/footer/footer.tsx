import { component$, Host, useStyles$ } from '@builder.io/qwik';
import styles from './footer.css?inline';

export default component$(
  () => {
    useStyles$(styles);

    return (
      <Host>
        <nav>
          <ul>
            <li>
              <a href="/">Home</a>
            </li>
            <li>
              <a href="/posts">Posts</a>
            </li>
            <li>
              <a href="/tags">Tags</a>
            </li>
            <li>
              <a href="/about">About</a>
            </li>
          </ul>
        </nav>
      </Host>
    );
  },
  {
    tagName: 'footer',
  }
);
