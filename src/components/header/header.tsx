import { component$, Host, useStyles$ } from '@builder.io/qwik';
import { useLocation } from '@builder.io/qwik-city';
import styles from './header.css?inline';

export default component$(
  (props: { fullWidth?: boolean }) => {
    useStyles$(styles);
    const pathname = useLocation().pathname;

    return (
      <Host class={{ 'full-width': !!props.fullWidth, 'site-header': true }}>
        <h1>
          <a href="/">Qwik City Blog 🤓</a>
        </h1>
        <nav>
          <a href="/posts" class={{ active: pathname === '/posts' }}>
            Posts
          </a>
          <a href="/tags" class={{ active: pathname === '/tags' }}>
            Tags
          </a>
          <a href="/about" class={{ active: pathname === '/about' }}>
            About
          </a>
        </nav>
      </Host>
    );
  },
  {
    tagName: 'header',
  }
);
