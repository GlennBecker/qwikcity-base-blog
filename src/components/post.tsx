import { component$, Host } from "@builder.io/qwik";
import { PostData } from "../types";

export const Post = component$((props: { post: PostData, slug: string }) => {
  const { slug, post } = props;
  const prettyDate = new Intl.DateTimeFormat('en-us', {
    dateStyle: 'long'
  }).format(new Date(post.date))
  return <Host>
    <header>
      <h3>{post.title}</h3>
      <p>Published <time dateTime={post.date}>{prettyDate}</time></p>
      <p>By {post.author}</p>
    </header>
    <p>{post.description}</p>
    <a href={`/post/${slug}`}>Continue Reading</a>
  </Host>
}, { tagName: 'li' })