import { component$ } from "@builder.io/qwik";
import { useLocation } from "@builder.io/qwik-city";

export default component$(() => {
  const location = useLocation().pathname;
  // Makes a post request to the theme API Endpoint
  // The form has a hidden field with the page location so we
  // can redirect back to the page the user was on
  // when they toggled
  return <form action="/api/theme" method="post">
    <input hidden={true} type="text" name="redirect" readOnly value={location} />
    <button>Toggle Theme</button>
  </form>
})