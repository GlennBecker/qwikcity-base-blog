---
title: "About"
---

## About Qwik City

QwikCity is a metaframe work for Qwik like NextJS is for React or SveleKit is for Svelte.

With QwikCity you can:

- Create routes by adding a .md, .mdx, .jsx or .tsx file in src/routes
- Create layouts by adding a _layout.tsx or _layout.jsx in the routes root or routes subdirectory directory you wish to create a layout for
- Create API endpoints by adding .js or .ts files in the routes folder. An example api route can be found in api/theme.

For more information check out [the docs](https://qwik.builder.io/docs/overview)