import { component$, Host, Slot } from '@builder.io/qwik';
import Footer from '../components/footer/footer';
import Header from '../components/header/header';
import ThemeToggle from '../components/theme-toggle/theme-toggle';

export default component$(() => {
  return (
    <Host id="root-content">
      <Header />
      <main>
        <Slot />
      </main>
      <ThemeToggle />
      <Footer />
    </Host>
  );
});
