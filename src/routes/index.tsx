import { component$, Host } from '@builder.io/qwik';
import { Post } from '../components/post';
import { frontmatter } from './stubFrontmatter';

export default component$(() => {
  return (
    <Host>
      <h2>Welcome to the QwikCity Blog!</h2>

      <p><em>Qwik</em> and informative posts about QwikCity</p>

      <div>
        <h2>Recent Posts</h2>
        <ul role='list'>
          {Object.entries(frontmatter).map(([slug, post]) => (
            <Post post={post} slug={slug} />
          ))}
        </ul>
      </div>
    </Host>
  );
});
