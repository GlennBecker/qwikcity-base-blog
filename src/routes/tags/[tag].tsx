import { component$, Host } from "@builder.io/qwik";
import { useLocation } from "@builder.io/qwik-city";
import { frontmatter } from "../stubFrontmatter";
import { Post } from "../../components/post";

export default component$(() => {
  const { params } = useLocation();
  const posts = Object.entries(frontmatter)
    .filter(([_, data]) => data.tags.includes(params.tag))
  return <Host>
    <h2>Showing posts tagged with {params.tag}</h2>
    <ul role='list'>
      {posts.map(([slug, post]) => <Post post={post} slug={slug} />)}
    </ul>
  </Host>
})