import { component$, Host } from "@builder.io/qwik";
import { frontmatter } from "../stubFrontmatter";

export default component$(() => {
  const tags: string[] = Object.values(frontmatter).reduce((all: string[], { tags }) => {
    return [...all, ...tags]
  }, [])
  return <Host>
    <h2>Tags</h2>
    <ul role='list'>
      {[...new Set(tags)].map(tag => <li>
        <a href={`/tags/${tag}`}>{tag}</a>
      </li>)}
    </ul>
  </Host>
})