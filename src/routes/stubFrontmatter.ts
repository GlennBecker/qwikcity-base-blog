import { PostData } from "../types"

export const DEFAULT: PostData = {
  title: 'untitled',
  author: 'anonymous',
  description: 'no description',
  date: new Date().toUTCString(),
  tags: []
}

export const frontmatter: Record<string, PostData> = {
  "api-endpoints": {
    "title": "API Endpoints",
    "description": "Handle HTTP Requests!",
    "author": "QwikCity",
    "date": "2022-07-01",
    "tags": ["resumability", "qwik"]
  },
  "layouts": {
    "title": "Layouts",
    "description": "Add Layouts For Your Routes and Pages!",
    "author": 'QwikCity',
    "date": '2022-07-01',
    "tags": ["resumability", "qwik", "layouts"]
  },
  "routes": {
    "title": 'Routing',
    "author": 'QwikCity',
    "date": '2022-07-01',
    "description": "Add Posts To Your New Blog!",
    "tags": ['qwik', 'routing', 'hydration']
  }
}