import { component$, Slot, Host, useStyles$ } from "@builder.io/qwik";
import { useDocumentHead, useLocation } from "@builder.io/qwik-city";
import { frontmatter, DEFAULT } from "../../../routes/stubFrontmatter";
import { PostData } from "../../../types";
import styles from './index.css?inline';
import { Upvote } from "../../../components/upvote/upvote";

export default component$(() => {
  useStyles$(styles)
  const head = useDocumentHead();
  const pathname = useLocation().pathname.replace('/post/', '');
  const { title, date, author, tags }: PostData = frontmatter[pathname] || DEFAULT;
  return <Host>
    <header>
      <h2>{title}</h2>
      <p>Published: <time>{new Date(date).toDateString()}</time></p>
      <p>By: {author}</p>
    </header>
    <Slot />
    <footer>
      <Upvote />
      <p>Tags</p>
      <ul role='list'>
        {tags.map(tag => <li><a href={`/tags/${tag}`}>{tag}</a></li>)}
      </ul>
    </footer>
  </Host>
}, { tagName: 'article' })