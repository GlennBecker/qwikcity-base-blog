---
title: "Layouts"
description: "Add Layouts For Your Routes and Pages!"
author: 'QwikCity'
date: '2022-07-01'
tags: ["resumability", "qwik", "layouts"]
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id turpis sit amet leo lacinia tincidunt. Nullam suscipit fringilla nisi commodo porta. Nulla facilisi. Phasellus mollis orci nec tempus volutpat. Quisque quis finibus nulla. Proin tincidunt enim semper justo porta, sed consectetur arcu vehicula. Nulla porttitor risus et vulputate congue. Morbi sagittis dolor a massa tempor porttitor. Suspendisse potenti. Cras maximus elit turpis, sit amet gravida lorem fringilla et. Quisque tincidunt luctus nunc.

Integer nec aliquam nunc, nec laoreet ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus risus eget semper efficitur. Etiam euismod vel lorem et vulputate. Maecenas congue accumsan nibh vitae vestibulum. Pellentesque sit amet efficitur ipsum, vel accumsan sem. Pellentesque bibendum risus maximus, tristique massa vel, vulputate odio.

Vestibulum vestibulum ac nisl non gravida. Ut interdum, lorem at hendrerit porttitor, neque quam venenatis tortor, a bibendum est ex ac enim. Aenean consequat, enim non sollicitudin egestas, purus lacus aliquet elit, id porttitor felis leo et nisl. Aenean dictum felis sit amet enim lacinia, at imperdiet massa facilisis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean imperdiet justo sed rhoncus sollicitudin. Etiam non tincidunt purus. Nunc vel pulvinar nibh. Donec tincidunt justo posuere turpis scelerisque porttitor. Etiam mauris urna, feugiat at metus sit amet, consectetur auctor erat. Duis a sapien augue.

For more information check out [the docs](https://qwik.builder.io/docs/overview)