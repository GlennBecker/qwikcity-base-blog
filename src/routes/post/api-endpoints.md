---
title: "API Endpoints"
description: "Handle HTTP Requests!"
author: 'QwikCity'
date: '2022-07-01'
tags: ["resumability", "qwik", "qwik-city"]
---

To add an API Endpoint just add a .ts or .js file in your routes!

Then export an EndpointHandler function with the name of the http method you'd like to handle

`
export const get: EndpointHandler = (ev: Ev) => {
  // do your magic
  return new Response(null, { status: 200 })
}
`

API endpoints can be plain functions or async. All standard http methods are supported; ie post, patch, delete, put, etc;

An example api route can be found in src/routes/api. We use this route to set a cookie, toggling the users theme preference.

For more information check out [the docs](https://qwik.builder.io/docs/overview)