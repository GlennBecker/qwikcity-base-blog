import { EndpointHandler } from "@builder.io/qwik-city";

export type ParsedCookie = Record<string, string>

export type Theme = 'light' | 'dark';

const COOKIE_KEY = 'qwikcity-blog-theme';
const MAX_AGE = 60; //Maximum age for the cookie in seconds

export const post: EndpointHandler = async (ev) => {
  const request = ev.request;
  const formdata = await ev.request.formData().catch((e) => {
    console.log(e);
    return new FormData();
  })
  const location = formdata.get('redirect')?.toString() || '/';
  const cookie: ParsedCookie = parseCookie(request.headers.get('cookie') || '');
  return new Response('', {
    status: 301,
    headers: {
      location,
      "Set-Cookie": `${COOKIE_KEY}=${getNewTheme(cookie)}; Max-Age=${MAX_AGE}`
    }
  })
}

const parseCookie = (raw: string): ParsedCookie => {
  if (!raw || raw === '') return { [COOKIE_KEY]: 'light' }
  return raw
    .split(';') // Seperate Key Value Pairs
    .map(kv => kv.split('=')) // Seperate Keys and Values
    .reduce((cookie: ParsedCookie, [k, v]) => {
      // Insert Key/Value Pairs into a Record
      cookie[k] = v;
      return cookie
    }, {})
}
const parseTheme = (raw: string): Theme => {
  return raw === 'dark' ? 'dark' : 'light'
}
const toggle = (theme: Theme): Theme => {
  const next = theme === 'light' ? 'dark' : 'light'
  return next
}
const getNewTheme = (cookie: { [key: string]: string }): Theme => {
  let themeValue = cookie[COOKIE_KEY] || '';
  console.log('Cookie Value', themeValue);
  let theme: Theme = parseTheme(themeValue)
  return toggle(theme)
}