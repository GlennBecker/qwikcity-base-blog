import { component$, Host } from "@builder.io/qwik";
import { useLocation } from "@builder.io/qwik-city";
import { Post } from "../../components/post";
import { frontmatter } from "../stubFrontmatter";

export const PAGINATION_SIZE = 2

export default component$(() => {
  const { params } = useLocation();
  const pageNumber = Number(params.page || "0");
  if (isNaN(pageNumber)) { return <Host><h2>Bad Request</h2></Host> }
  const paginationStart = pageNumber * PAGINATION_SIZE;
  const paginationEnd = paginationStart + PAGINATION_SIZE;
  const prev = pageNumber === 0 ? null : pageNumber - 1;
  const next = pageNumber + 1
  const totalPages = Object.keys(frontmatter).length;
  return <Host>
    <h2>Posts</h2>
    <p>You are on page {pageNumber}</p>
    <p>Showing posts {paginationStart} - {paginationEnd}</p>
    <p>Tweek the pagination size in <code>src/route/posts/[...page].tsx</code></p>
    <ul role='list'>
      {Object.entries(frontmatter)
        .slice(paginationStart, paginationEnd)
        .map(([slug, post]) => <Post slug={slug} post={post} />)}
    </ul>
    <nav>
      <ul>
        {prev !== null
          ? <li><a href={`/posts/${prev === 0 ? '' : prev}`}>Newer</a></li>
          : 'Newer'
        }
        {next * PAGINATION_SIZE < totalPages
          ? <li><a href={`/posts/${next}`}>Older</a></li>
          : 'Older'
        }
      </ul>
    </nav>
  </Host>
})