export interface PostData {
  title: string
  author: string
  description: string
  date: string,
  tags: string[]
}